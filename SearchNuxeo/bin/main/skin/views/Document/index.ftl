<@extends src="masterPage.ftl">

<@block name="content">
	 <div class="row">
    	<div class="col-md-6 col-md-offset-3">
   			
   				<h2>Now you are working in ${Document.title} document</h2>
 				<h5>Created: ${Document["dc:created"]}</h5>
 				<h5>Last modified: ${Document["dc:modified"]}</h5>
 				<hr>
 				<table class="table table-striped table-bordered">
 				<#if Document.type == "Visitka">
 				
 					<tr>
 						<td>Title:</td>  <td>${Document["dc:title"]}</td>
 					</tr>
 		
 					<tr>
 						<td>Name:</td>  <td>${Document["visitka:Name"]}</td>
 					</tr>
 		
 					<tr>
 						<td>Phone:</td>  <td>${Document["visitka:Phone"]}</td>
 					</tr>
 				<#else>
 				<#list Document.schemas as schema>
 						<tr>
 							<td>${schema}</td>
 						</tr>
 					</#list>
 			</#if>
 			</table>
   		</div>
 	</div>

</@block>
</@extends>