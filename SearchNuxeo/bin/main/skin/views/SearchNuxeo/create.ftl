<@extends src="masterPage.ftl">

<@block name="content">

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h3>Now you should create document!</h3>
		<form action="create" method="POST">
		
			  <div class="form-group">
    			<label for="schema">Schema</label>
    			<input type="text" required=required class="form-control" id="schema" placeholder="Schema: dublincore, common, yourSchema, etc..." name="schema">
 			 </div>
 			 <div class="form-group">
    			<label for="documentRoot">Name</label>
    			<input type="text" required=required class="form-control" id="documentRoot" placeholder="Example root: /default-domain/workspaces/FindDocument/... " name="documentRoot">
 			 </div>
 			 
			 <div class="form-group">
    			<label for="title">Title</label>
    			<input type="text" required=required class="form-control" id="title" placeholder="Title..." name="title">
 			 </div>
 			 <div class="form-group">
    			<label for="name">Name</label>
    			<input type="text" required=required class="form-control" id="name" placeholder="Name..." name="name">
 			 </div>
 			 <div class="form-group">
    			<label for="phone">Phone</label>
    			<input type="text" required=required class="form-control" id="phone" placeholder="Phone..." name="phone">
 			 </div>
 			 <input class="btn btn-primary" type="submit" value="Create" >
		</form>
	</div>
</div>
</@block>
</@extends>