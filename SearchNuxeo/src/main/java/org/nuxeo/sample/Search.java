package org.nuxeo.sample;

import org.nuxeo.ecm.automation.client.model.PropertyList;
import org.nuxeo.ecm.automation.client.model.PropertyMap;

public class Search {
	private String name;
	private String path;
	private String type;
	
	public Search(String Name, String Path, String Type) {
		this.name=Name;
		this.path=Path;
		this.type=Type;
		
	}
	public String toString(){
		return "Name: "+ this.name + " Path: " + this.path + " Type: "+ this.type;
	}
	
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
}
