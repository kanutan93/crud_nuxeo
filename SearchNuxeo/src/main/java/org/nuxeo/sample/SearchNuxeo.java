/**
 * 
 */

package org.nuxeo.sample;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import org.nuxeo.ecm.automation.client.Constants;
import org.nuxeo.ecm.automation.client.Session;
import org.nuxeo.ecm.automation.client.adapters.DocumentService;
import org.nuxeo.ecm.automation.client.jaxrs.impl.HttpAutomationClient;
import org.nuxeo.ecm.automation.client.model.Document;
import org.nuxeo.ecm.automation.client.model.Documents;
import org.nuxeo.ecm.core.rest.DocumentFactory;
import org.nuxeo.ecm.webengine.model.WebObject;
import org.nuxeo.ecm.webengine.model.impl.ModuleRoot;
import com.hp.hpl.jena.sparql.function.library.e;
import org.nuxeo.ecm.webengine.model.exceptions.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import org.nuxeo.ecm.core.rest.*;
import org.nuxeo.ecm.webengine.model.impl.*;
import org.nuxeo.ecm.webengine.model.*;

/**
 * The root entry for the WebEngine module. 
 * @author dmitrii
 */
@Path("/SearchNuxeo")
@Produces("text/html;charset=UTF-8")
@WebObject(type="SearchNuxeo")
public class SearchNuxeo extends ModuleRoot {

    @GET
    public Object doGet() {
        return getView("index");
    }
    @POST
    @Path("/")
    public Object doPost( @FormParam("searchText") String searchText,
    					  @FormParam("searchBy") String searchBy ) throws IOException{
    	ArrayList<Search> search= new ArrayList<Search>();
    	HttpAutomationClient client=new HttpAutomationClient("http://localhost:8080/nuxeo/site/automation");
    	Session session = client.getSession("Administrator", "Administrator");
    	Documents docs;
    	if(searchBy.equals("ecm:fulltext")){
    		docs = (Documents) session.newRequest("Document.Query").setHeader(Constants.HEADER_NX_SCHEMAS, "*").set("query", "SELECT * FROM Document WHERE "+searchBy+" = '"+searchText+"' ").execute();
    	}
    	else{
    		docs = (Documents) session.newRequest("Document.Query").setHeader(Constants.HEADER_NX_SCHEMAS, "*").set("query", "SELECT * FROM Document WHERE "+searchBy+" LIKE '%"+searchText+"%' ").execute();
    	}
    	
    	for(Document e: docs){
    		search.add( new Search (e.getTitle(),e.getPath(),e.getType() ) );
    	}
    	client.shutdown();
		return getView("index").arg("search",search);
    }
    
    @Path("search/{document}")
    public Object getRepositoryView(@PathParam("document") String document) {
    	
      return DocumentFactory.newDocument(ctx, document);
    }
    
    @GET
    @Path("create")
    	public Object createDocumentGet(){
    	return getView("create");
    }
    @POST
    @Path("create")
    public void createDocumentPost(@FormParam("title") String title,
    							   @FormParam("name") String name,
    							   @FormParam("phone") String phone,
    							   @FormParam("schema") String schema,
    							   @FormParam("documentRoot") String documentRoot) throws IOException{
    	HttpAutomationClient client=new HttpAutomationClient("http://localhost:8080/nuxeo/site/automation");
    	Session session = client.getSession("Administrator", "Administrator");
    	Document root = (Document) session.newRequest("Document.Fetch").set("value", documentRoot).execute();
    	Document document = new Document(title, schema);
    	document.set("dc:title", title);
    	document.set("visitka:Name", name);
    	document.set("visitka:Phone", phone);
    	DocumentService documentService = session.getAdapter(DocumentService.class);
    	document = documentService.createDocument(root.toString(), document);
    	client.shutdown();
    }
    
    /*public Response handleError(WebApplicationException e) {
        if (e instanceof WebApplicationException) {
          return Response.status(500).entity(getTemplate("error/error_500.ftl")).build();
        }
        else
          return (Response) super.handleError(e);
        }*/
}
