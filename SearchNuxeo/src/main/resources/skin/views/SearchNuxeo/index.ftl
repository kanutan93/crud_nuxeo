<@extends src="masterPage.ftl">

<@block name="content">
<div class="row">
	<div class="col-md-8 col-md-offset-2">
<form action="SearchNuxeo" method="POST">
	<p class="lead"> Search by </p>
  	<input type="radio" name="searchBy"  value="ecm:name" checked/> Name
 	<input type="radio" name="searchBy"  value="ecm:fulltext"/> Content
  	<input type="radio" name="searchBy" value="ecm:primaryType"/> Type
  	<hr>

	<input class="form-control" type="text" required=required placeholder="Input..." name="searchText"/> <br>
	<input class="btn btn-success" type="submit" value="Search"/><br>
	
</form>

<#if search>
<table class="table table-striped">
	<tr>
		<th>Name</th>
		<th>Path</th>
		<th>Type</th>
	</tr>
<#list search as searchElement>
	<tr>
		<td> <a href="${This.path}/search${searchElement.path}"> ${searchElement.name} </a> </td>
		<td>${searchElement.path}</td>
		<td>${searchElement.type}</td>
	</tr>
</#list>

</table>
</#if>
	</div>
</div>
</@block>
</@extends>
